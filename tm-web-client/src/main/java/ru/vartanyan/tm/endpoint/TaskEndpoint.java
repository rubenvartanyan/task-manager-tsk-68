package ru.vartanyan.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vartanyan.tm.api.endpoint.ITaskEndpoint;
import ru.vartanyan.tm.api.service.ITaskService;
import ru.vartanyan.tm.model.Task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskEndpoint implements ITaskEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return new ArrayList<>(taskService.findAll());
    }

    @Override
    @GetMapping("/find/{id}")
    public Task find(@PathVariable("id") String id) {
        return taskService.findById(id);
    }

    @Override
    @PostMapping("/create")
    public Task create(@RequestBody Task task) {
        taskService.add(task);
        return task;
    }

    @Override
    @PostMapping("/createAll")
    public List<Task> createAll(@RequestBody List<Task> tasks) {
        for (Task task: tasks) {
            taskService.add(task);
        }
        return tasks;
    }

    @Override
    @PutMapping("/save")
    public Task save(@RequestBody Task task) {
        taskService.add(task);
        return task;
    }

    @Override
    @PutMapping("/saveAll")
    public List<Task> saveAll(@RequestBody List<Task> tasks) {
        for (Task task: tasks) {
            taskService.add(task);
        }
        return tasks;
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") String id) {
        taskService.removeById(id);
    }

    @Override
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        final Collection<Task> tasks = taskService.findAll();
        for (Task task: tasks) {
            taskService.removeById(task.getId());
        }
    }

}

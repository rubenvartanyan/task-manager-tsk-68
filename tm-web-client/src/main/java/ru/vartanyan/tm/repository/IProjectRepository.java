package ru.vartanyan.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vartanyan.tm.model.Project;

public interface IProjectRepository extends JpaRepository<Project, String> {

}

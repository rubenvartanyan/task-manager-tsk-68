package ru.vartanyan.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.vartanyan.tm.model.Task;

import java.util.List;


public interface TaskRestEndpointClient {

    static TaskRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter =
                new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters =
                new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory =
                () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskRestEndpointClient.class, "http://localhost:8080/api/tasks");
    }

    @GetMapping("/findAll")
    public List<Task> findAll();

    @GetMapping("/find/{id}")
    public Task find(@PathVariable("id") final String id);

    @PostMapping("/create")
    public Task create(@RequestBody final Task task);

    @PostMapping("/createAll")
    public List<Task> createAll(@RequestBody final List<Task> tasks);

    @PutMapping("/save")
    public Task save(@RequestBody final Task task);

    @PutMapping("/saveAll")
    public List<Task> saveAll(@RequestBody final List<Task> tasks);

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") final String id);

    @DeleteMapping("/deleteAll")
    public void deleteAll();

}

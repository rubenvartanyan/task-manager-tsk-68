package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.model.Project;

public interface IProjectService extends IRecordService<Project> {

}
